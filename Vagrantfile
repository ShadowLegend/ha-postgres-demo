# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  config.vm.box = "ubuntu/bionic64"

  config.vm.provider "virtualbox" do |vb|
    vb.cpus = 1
    vb.memory = "1024"
  end
  
  config.vm.define "consul-0" do |node|
    node.vm.hostname = "consul-0"
    node.vm.network "private_network", ip: "10.1.0.20", netmask: "255.255.255.192"
    node.vm.network "public_network",
      bridge: ["en0", "en0: wifi (AirPort)"],
      ip: "69.6.9.3",
      netmask: "255.255.255.240"
    node.vm.synced_folder "./shared", "/shared"

    node.vm.provision "ansible" do |ansible|
      ansible.playbook = "consul.yml"
      ansible.extra_vars = {
        node_internal_ip: "10.1.0.20",
        consul_ips: [
          "10.1.0.20",
          "10.1.0.21",
          "10.1.0.22"
        ]
      }
    end
  end
  config.vm.define "consul-1" do |node|
    node.vm.hostname = "consul-1"
    node.vm.network "private_network", ip: "10.1.0.21", netmask: "255.255.255.192"
    node.vm.network "public_network",
      bridge: ["en0", "en0: wifi (AirPort)"],
      ip: "69.6.9.4",
      netmask: "255.255.255.240"
    node.vm.synced_folder "./shared", "/shared"

    node.vm.provision "ansible" do |ansible|
      ansible.playbook = "consul.yml"
      ansible.extra_vars = {
        node_internal_ip: "10.1.0.21",
        consul_ips: [
          "10.1.0.20",
          "10.1.0.21",
          "10.1.0.22"
        ]
      }
    end
  end
  config.vm.define "consul-2" do |node|
    node.vm.hostname = "consul-2"
    node.vm.network "private_network", ip: "10.1.0.22", netmask: "255.255.255.192"
    node.vm.network "public_network",
      bridge: ["en0", "en0: wifi (AirPort)"],
      ip: "69.6.9.5",
      netmask: "255.255.255.240"
    node.vm.synced_folder "./shared", "/shared"

    node.vm.provision "ansible" do |ansible|
      ansible.playbook = "consul.yml"
      ansible.extra_vars = {
        node_internal_ip: "10.1.0.22",
        consul_ips: [
          "10.1.0.20",
          "10.1.0.21",
          "10.1.0.22"
        ]
      }
    end
  end

  config.vm.define "haproxy" do |node|
    node.vm.hostname = "haproxy"
    node.vm.network "forwarded_port", guest: 6000, host: 16000
    node.vm.network "forwarded_port", guest: 7000, host: 17000
    node.vm.network "private_network", ip: "10.1.0.30", netmask: "255.255.255.192"
    node.vm.network "public_network",
      ip: "110.10.0.2",
      bridge: ["en0", "en0: wifi (AirPort)"],
      netmask: "255.255.255.252"
    node.vm.synced_folder "./shared", "/shared"

    node.vm.provision "ansible" do |ansible|
      ansible.playbook = "haproxy.yml"
      ansible.extra_vars = {
        node_internal_ip: "10.1.0.30",
        pg_ips: [
          "10.1.0.10",
          "10.1.0.11",
          "10.1.0.12"
        ],
        consul_ips: [
          "10.1.0.20",
          "10.1.0.21",
          "10.1.0.22"
        ]
      }
    end
  end
  
  config.vm.define "pg-0" do |node|
    node.vm.hostname = "pg-0"
    node.vm.network "private_network", ip: "10.1.0.10", netmask: "255.255.255.192"
    node.vm.network "public_network",
      bridge: ["en0", "en0: wifi (AirPort)"],
      ip: "169.6.9.3",
      netmask: "255.255.255.240"
    node.vm.synced_folder "./shared", "/shared"

    node.vm.provision "ansible" do |ansible|
      ansible.playbook = "pg.yml"
      ansible.extra_vars = {
        node_internal_ip: "10.1.0.10",
        consul_ips: [
          "10.1.0.20",
          "10.1.0.21",
          "10.1.0.22"
        ]
      }
    end
  end
  config.vm.define "pg-1" do |node|
    node.vm.hostname = "pg-1"
    node.vm.network "private_network", ip: "10.1.0.11", netmask: "255.255.255.192"
    node.vm.network "public_network",
      bridge: ["en0", "en0: wifi (AirPort)"],
      ip: "169.6.9.4",
      netmask: "255.255.255.240"
    node.vm.synced_folder "./shared", "/shared"

    node.vm.provision "ansible" do |ansible|
      ansible.playbook = "pg.yml"
      ansible.extra_vars = {
        node_internal_ip: "10.1.0.11",
        consul_ips: [
          "10.1.0.20",
          "10.1.0.21",
          "10.1.0.22"
        ]
      }
    end
  end
  config.vm.define "pg-2" do |node|
    node.vm.hostname = "pg-2"
    node.vm.network "private_network", ip: "10.1.0.12", netmask: "255.255.255.192"
    node.vm.network "public_network",
      bridge: ["en0", "en0: wifi (AirPort)"],
      ip: "169.6.9.5",
      netmask: "255.255.255.240"
    node.vm.synced_folder "./shared", "/shared"

    node.vm.provision "ansible" do |ansible|
      ansible.playbook = "pg.yml"
      ansible.extra_vars = {
        node_internal_ip: "10.1.0.12",
        consul_ips: [
          "10.1.0.20",
          "10.1.0.21",
          "10.1.0.22"
        ]
      }
    end
  end
end