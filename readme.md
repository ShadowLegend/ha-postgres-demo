## High Availability Postgres Demo
As for demo, we will only open source tool to achieve HA setup for postgres.

The demo will include both setup in docker based using docker swarm and non docker based.

### Requirement
- `vagrant` >= 2.2.7
- `mini-conda` >= 4.8.0

## Tech Stack
HAProxy - Reliable and high performance load balancer
We will use `haproxy` to load balance traffic to multiple nodes

NOTE: Recommendation in real production would be would use aws ELB or google cloud loading balancing service, but you can use `haproxy` as well but you have to maintain your load balancer yourself

Consul - Modern service discovery and key value store
We will use `consul` as distributed lock tool and consensus management

NOTE: There are many dcs tool such as `etcd`, `zookeeper` and you know the rest. `patroni` also support many different dcs tool

Patroni - HA postgres template
This is the main tool to accomplish HA setup for postgres

Barman - Postgres backup manager (additional)
We will also use `barman` to setup auto backup manager to backup postgres in cluster mode

Vagrant - Tool to build and manage virtual machine in development environment
Creating google cloud or aws account for this demo or even writing `terraform` and `ansible` to automate still take lots of time so `vagrant` can be use to create multiple machines with different configuration(machine spec, basic networking etc)
