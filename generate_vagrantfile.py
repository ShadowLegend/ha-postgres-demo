'''
generate vagrant file dynamically
'''

from jinja2 import Environment, FileSystemLoader

def parse_and_output_template_file(env, template_file_path, output_file_path, **kargs):
    '''parse and output template file'''
    template = env.get_template(template_file_path)
    parsed_template = template.render(**kargs)

    with open(output_file_path, 'w+') as vagrantfile:
        vagrantfile.write(parsed_template)

def generate_vagrantfile(env):
    '''parse and output Vagrantfile'''
    template_args = {
        'pg_count': 3,
        'consul_count': 3,
        'haproxy_count': 1}

    parse_and_output_template_file(
        env=env,
        output_file_path='Vagrantfile',
        template_file_path='./Vagrantfile.j2',
        **template_args)

def main():
    '''output Vagrantfile and consul playbook'''
    env = Environment(loader=FileSystemLoader('templates'))

    generate_vagrantfile(env)

main()
