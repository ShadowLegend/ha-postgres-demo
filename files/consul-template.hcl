consul {
  address = "localhost:8500"

  retry {
    attempts = 12
    enabled = true
    backoff = "250ms"
  }
}

max_stale = "10m"
log_level = "warn"

reload_signal = "SIGHUP"
kill_signal = "SIGINT"

pid_file = "/var/lib/consul-template/consul-template.pid"

wait {
  min = "5s"
  max = "10s"
}

deduplicate {
  enabled = true
}

template {
  source = "/var/lib/consul-template/templates/haproxy.tmpl"
  destination = "/etc/haproxy/haproxy.cfg"

  command = "systemctl restart haproxy"

  perms = 0400

  left_delimiter  = "{{"
  right_delimiter = "}}"

  wait {
    min = "2s"
    max = "10s"
  }
}